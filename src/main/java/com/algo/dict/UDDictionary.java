package com.algo.dict;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * SCWS is a word segment software, 
 * here use Chinese dictionary of SCWS <br/>
 * format like below: <br/>
 * 		大      a       144099
		多      a       98900
		好      a       92543
		新      a       62626
		小      a       57969

 * 		...
 * 
 * @author lujianfeng@miaozhen.com
 *
 */

public class UDDictionary implements Dictionary{
	
	private Map<String, String> dict;
	
	public UDDictionary(String path){
		dict = new HashMap<String, String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while((line = br.readLine()) != null){
				String[] items = line.split("\\t");
				dict.put(items[0], items[1]);
			}
			br.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public UDDictionary(String[] path){
		dict = new HashMap<String, String>();
		for(String file : path){
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while((line = br.readLine()) != null){
					String[] items = line.split("\\t");
					dict.put(items[0], items[1]);
				}
				br.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public boolean inDictionary(String word) {
		return dict.containsKey(word);
	}

	public String getPOS(String word) {
		String value = dict.get(word);
		if(value == null)	return UNDEFINED;
		return value;
	}

	public static void main(String[] args) {
		//for test
		Dictionary dict = new SCWSDictionary("E://test//dict.utf8");
		System.out.println(dict.inDictionary("中华人民共和国"));
		System.out.println(dict.getPOS("澳门"));
	}

}
