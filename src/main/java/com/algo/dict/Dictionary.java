package com.algo.dict;

public interface Dictionary {
	
	/**
	 *  undefined word POS in dictionary
	 */
	public static final String UNDEFINED = "nw";
	
	/**
	 * find whether the word in dictionary or not
	 */
	public boolean inDictionary(String word);
	
	/**
	 * get the part of speech (POS) of word
	 */
	public String getPOS(String word);
	

}