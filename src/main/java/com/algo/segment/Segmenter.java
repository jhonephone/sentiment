package com.algo.segment;

import java.util.ArrayList;
import java.util.List;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.ansj.util.MyStaticValue;

import com.algo.constant.Constants;

/**
 * segment Chinese words
 * @author lujianfeng@miaozhen.com
 * 
 */

public class Segmenter {
	
	//force to pass user dictionary path
	public Segmenter(String userDict){
		MyStaticValue.userLibrary = userDict;
	}
	
	public List<String> segment(String line){
		List<String> words = new ArrayList<String>();
		for(Term term : ToAnalysis.parse(line)){
			words.add(term.getName());
		}
		return words;
	}
	
	/*public static void segment(String[] inputs, String out){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(out));
			String line;
			for(String in : inputs){
				try {
					BufferedReader br = new BufferedReader(new FileReader(in));
					while((line = br.readLine()) != null){
						List<Term> terms = ToAnalysis.parse(line) ;
						bw.write(terms.toString()+"\n");
					}
					br.close();
				} catch (IOException e) {
					throw new RuntimeException();
				}
			}
			bw.close();
		} catch (IOException e1) {
			throw new RuntimeException();
		}
	}*/
	
	public static void main(String[] args) {
		String line = "泰国公布爆炸赔偿标准：外籍遇难者最高5.4万元";
		new Segmenter(Constants.SCWSDICTIONARY).segment(line);
	}

}
