package com.algo.word;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * save stop words, filter stop words
 * @author supertool
 *
 */
public class StopWords {
	
	private Set<String> stopWords;

	public StopWords(String path){
		//read stop words file
		stopWords = new HashSet<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while((line = br.readLine()) != null){
				stopWords.add(line);
				if(line.length() == 1) System.out.print(line);
			}
			br.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * whether word contains stop words or not
	 */
	public boolean containsStopWords(String word){
		
		for(int j = 1; j < word.length(); j++){
			if(stopWords.contains(word.substring(0, j)))
				return true;
		}
		for(int j = 1; j < word.length(); j++){
			if(stopWords.contains(word.substring(j, word.length())))
				return true;
		}
		
		return false;
	}
	
	public static String getSplitWords(String path){
		try{
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line, splitWords = "";
			while((line = br.readLine()) != null){
				splitWords += line;
			}
			return splitWords;
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String word = "突然听";
		System.out.println(new StopWords("E://test//stoplist.utf8").containsStopWords(word));
	}

}
