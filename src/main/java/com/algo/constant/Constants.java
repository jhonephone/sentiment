package com.algo.constant;

public class Constants {

	public static final String SPLITWORDSPATH = "library/userLibrary/splitwords.utf8";
	public static final String NAGAOFILTER = "20,3,3,5";
	public static final String NEWWORDSFILTER = "100,40";
	
	public static final int NGRAM = 5;
	
	public static final String SCWSDICTIONARY = "library/userLibrary/dict.utf8";
}
