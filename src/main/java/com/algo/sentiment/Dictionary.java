package com.algo.sentiment;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by supertool on 2014/10/29.
 */
public class Dictionary {
    private String filesPath;
    private Map<String,Integer> dictFilesMap;
    private String userDict;

    public String getFilesPath() {
        return filesPath;
    }

    public void setFilesPath(String filesPath) {
        this.filesPath = filesPath;
    }

    public Map<String, Integer> getDictFilesMap() {
        return dictFilesMap;
    }

    public void setDictFilesMap(Map<String, Integer> dictFilesMap) {
        this.dictFilesMap = dictFilesMap;
    }

    public String getUserDict() {
        return userDict;
    }

    public void setUserDict(String userDict) {
        this.userDict = userDict;
    }

    public Dictionary(String propertyDic) {
        this.dictFilesMap = new LinkedHashMap<String, Integer>();
        InputStream is = null;
        BufferedReader bufferedReader = null;
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            is = classloader.getResourceAsStream(propertyDic);
            bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = bufferedReader.readLine();
            while (line != null) {
                parsePathString(line);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void parsePathString(String line) {
        if (line.startsWith("#")) {
            return;
        }
        String[] kv = line.trim().split("=");
        if (kv.length != 2) {
            return;
        }

        String fileSeparator = "/";
        kv[0] = kv[0].trim();
        kv[1] = kv[1].trim();
        if (kv[0] .equals("PATH")) {
            this.filesPath = kv[1];
        }

        else if(kv[0].equals("USER_DIC")) {
            this.userDict = this.filesPath + fileSeparator + kv[1];
        }
        else if (kv[0] .equals( "POSITIVE_WORD_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], 1);
        }
        else if (kv[0] .equals( "NEGATIVE_WORD_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], -1);
        }
        else if (kv[0] .equals( "NOT_WORD_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], 0);
        }
        else if (kv[0] .equals( "PUNCTUATION_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], 3);
        }
        else if (kv[0] .equals( "DEGREE_WORD_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], 2);
        }
        else if (kv[0] .equals( "NOISE_WORD_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], 4);
        }
        else if (kv[0] .equals( "QUESTION_WORD_DIC")) {
            this.dictFilesMap.put(this.filesPath + fileSeparator +kv[1], 6);
        }

    }

    public static void main(String[] args) {
        String propertyFile = "dict.properties";
        Dictionary dictionary = new Dictionary(propertyFile);
        System.out.println(dictionary.getDictFilesMap());
        Sentiment sentiment = new Sentiment(dictionary.getDictFilesMap());
        System.out.println(sentiment.getWordItemMap().size());
    }

}
