package com.algo.sentiment;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by supertool on 2014/10/29.
 */
public class Sentiment {
    private Map<String, Item> wordItemMap;
    private Double factor;

    public Map<String, Item> getWordItemMap() {
        return wordItemMap;
    }

    public void setWordItemMap(Map<String, Item> wordItemMap) {
        this.wordItemMap = wordItemMap;
    }

    public Sentiment(Map<String, Integer> map) {
        this.wordItemMap = new HashMap<String, Item>();
        this.factor = 0.3;
        for (String file : map.keySet()) {
            readItemsFromFile(file, map.get(file));
        }
    }

    private void readItemsFromFile(String file,Integer groupId) {
        InputStream is = null;
        BufferedReader bufferedReader = null;
        System.out.println(file);
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            System.out.println(classloader.getResource(file));
            is = classloader.getResourceAsStream(file);
            bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = bufferedReader.readLine();
            while (line != null) {
//                System.out.println(line);
                parseItemString(line, groupId);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void parseItemString(String line, Integer groupId) {
        String[] words = line.trim().split("\\s+");
        Integer size = words.length;
        if (size == 2) {
            this.wordItemMap.put(words[0], new Item(groupId, Double.parseDouble(words[1])));
        }
        else if (size == 1) {
            this.wordItemMap.put(words[0], new Item(groupId, defaultScore(groupId)));
        }
    }
    private Double defaultScore(Integer gId) {
        Double ans = 0.0;
        switch (gId) {
            case -1:
                ans = -1 * this.factor;
                break;
            case 1:
                ans = this.factor;
                break;
            case 2:
                ans = 1.5;
                break;
            default:
                break;
        }
        return ans;
    }


}
