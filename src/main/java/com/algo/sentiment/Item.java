package com.algo.sentiment;

/**
 * Created by supertool on 2014/10/29.
 */
public class Item {
    private Integer groupId;
    private Double weight;

    public Item(Integer groupId, Double weight) {
        this.groupId = groupId;
        this.weight = weight;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;

        Item item = (Item) o;

        if (!groupId.equals(item.groupId)) return false;
        if (!weight.equals(item.weight)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupId.hashCode();
        result = 31 * result + weight.hashCode();
        return result;
    }
}
