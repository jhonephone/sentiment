package com.algo.sentiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by zrf on 8/15/15.
 */
public class SentimentUtils {

    private static class SentimentMatchInstance {
        private static final SentimentMatch instance = new SentimentMatch("dict.properties");
    }

    public static SentimentMatch getInstance() {
        return SentimentMatchInstance.instance;
    }

    private SentimentUtils() {
    }


    public static void main(String[] args) throws IOException {
        SentimentMatch sent = SentimentUtils.getInstance();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String line = reader.readLine();

        while (line != null) {
            Integer tag = sent.sentimentDegree(line);
            System.out.println(tag);
            line = reader.readLine();
        }
    }
}



